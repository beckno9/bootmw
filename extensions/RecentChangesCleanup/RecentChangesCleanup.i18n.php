<?php

$messages = array ();

/* *** English *** */
$messages['en'] = array (
  'recentchangescleanup' => 'Recent Changes Cleanup',
  'rc-cleanup-desc' => 'Gives the option to an admin or user to mark a line in the Recent Changes as coming from a bot. This hides it in the default view of Recent Changes.',
  'rc-cleanup-access-error' => 'Access to this maintenance related page is limited to admins and editors with atleast $1 edits or with the RecentChangesCleanup right.',
  'rc-cleanup-show' => 'Show',
  'rc-cleanup-hide' => 'Hide',
  'rc-cleanup-header-user' => 'User who performed action',
  'rc-cleanup-header-comment' => 'Comment',
  'rc-cleanup-header-action' => 'User who performed action',
  'right-recentchangescleanup' => 'Clean up [[Special:RecentChanges|Recent changes]] from actions that need to be hidden from the default view.',
  
); 


/* *** Russian *** */
$messages['ru'] = array (
  'recentchangescleanup' => 'Скрытие недавних правок',
  'rc-cleanup-desc' => 'Предоставляет возможность участникам выборочно отмечать правки как совершенные ботом. Данное действие скрывает правку со списка свежих правок.',
  'rc-cleanup-access-error' => 'Доступ к этой странице имеют лишь участники, совершившие как минимум $1 правок или имеющие соответствующие права.',
  'rc-cleanup-show' => 'Показать',
  'rc-cleanup-hide' => 'Скрыть',
  'rc-cleanup-header-user' => 'Пользователь, совершивший действие',
  'rc-cleanup-header-comment' => 'Комментарий',
  'rc-cleanup-header-action' => 'Пользователь, совершивший действие',
  'right-recentchangescleanup' => 'Возможность отметки действий в списке [[Служебная:Свежие правки|свежих правок]] как скрытых по умолчанию',

);

